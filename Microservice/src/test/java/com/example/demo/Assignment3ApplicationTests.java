package com.example.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Assignment3ApplicationTests {

	static final String URL_COURSE = "http://localhost:8900/course";
	
	@Autowired
	CourseController controller;


	@Test
	public void getMehod() {

//		HttpHeaders headers = new HttpHeaders();
//		headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
//		headers.setContentType(MediaType.APPLICATION_JSON);
//		HttpEntity<CourseBean[]> entity = new HttpEntity<CourseBean[]>(headers);
//
//		RestTemplate restTemplate = new RestTemplate();
//
//		ResponseEntity<CourseBean[]> response = restTemplate.exchange(URL_COURSE, //
//				HttpMethod.GET, entity, CourseBean[].class);
//
//		CourseBean[] result = response.getBody();


		List<CourseBean> result = controller.getMethod();
		
		if(result.size()!=0) {
			Assert.assertTrue(true);
		}else {

			Assert.fail();
		}

	}

	@Test
	public void postMethod()  {
		// TODO Auto-generated method stub

		CourseBean cb = new CourseBean();

		cb.setId(5);
		cb.setPrice(1000);
		cb.setStartDate("2012-11-11");
		cb.setEnddate("2013-11-11");
		cb.setTitle("CS");



//		HttpHeaders headers = new HttpHeaders();
//		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
//		headers.setContentType(MediaType.APPLICATION_JSON);
//
//		RestTemplate restTemplate = new RestTemplate();
//
//		HttpEntity<CourseBean> requestBody = new HttpEntity<>(cb, headers);
//		String e = restTemplate.postForObject(URL_COURSE, requestBody, String.class);

		
		String e = controller.postMethod(cb);
		
		Assert.assertEquals("Sucessful", e);


	}

	@Test
	public void getByIdMethod() {
		// TODO Auto-generated method stub

//		HttpHeaders headers = new HttpHeaders();
//		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE );
//		headers.setContentType(MediaType.APPLICATION_JSON);
//		HttpEntity<CourseBean> entity = new HttpEntity<CourseBean>(headers);
//
//		RestTemplate restTemplate = new RestTemplate();
//
//		ResponseEntity<CourseBean> response = restTemplate.exchange(URL_COURSE+"/1", //
//				HttpMethod.GET, entity, CourseBean.class);

		
		
		Optional<CourseBean> result = controller.putMethod(1);


			Assert.assertTrue((result.get().getId() == 1));


	}


	@Test
	public void deleteMethod() {
		// TODO Auto-generated method stub


//		RestTemplate restTemplate = new RestTemplate();

		//	    try {
		//	    	restTemplate.delete(URL_COURSE,map);
		//	    }catch(Exception e)
		//	    {
		//	    	Assert.fail();
		//	    	
		//	    }

//		ResponseEntity<String> re = restTemplate.exchange(URL_COURSE+"/5", HttpMethod.DELETE, null, String.class);
		
		String re = controller.deleteMethod(5);
		Assert.assertTrue(re.equals("Deleted"));
	}



}
