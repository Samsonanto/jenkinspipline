package com.example.demo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="course")
public class CourseBean {

	@Column
	private String title;

	@Id
	private int id;

	@Column
	private long price;

	@Column
	private String startdate;

	@Column
	private String enddate;


	public CourseBean() {
		super();
	}
	public CourseBean(String title, long price, String startdate, String enddate) {
		super();
		this.title = title;
		this.id = id;
		this.price = price;
		this.startdate = startdate;
		this.enddate = enddate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getPrice() {
		return price;
	}
	public void setPrice(long price) {
		this.price = price;
	}
	public String getStartDate() {
		return startdate;
	}
	public void setStartDate(String startDate) {
		this.startdate = startDate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	@Override
	public String toString() {
		return "CourseBean [title=" + title + ", id=" + id + ", price=" + price + ", startDate=" + startdate
				+ ", enddate=" + enddate + "]";
	}









}
